const router = require('express').Router();;
const request = require('request');
const {CLOVER_CASES_URL} = require('../../config.js');

router.get('/', (req, res) => {
  request(`${CLOVER_CASES_URL}/cases/all`, (err, body, data) => {
    res.json(JSON.parse(data));
  })
})

module.exports = router;
