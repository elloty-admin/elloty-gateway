require('dotenv').config();
const router = require('express').Router();;
const auth = require('./auth');
const clover = require('./clover/index');

router.use('/auth',auth);
router.use('/clover',clover)

module.exports = router;
