const {AUTH_URL} = require('../config');
const router = require('express').Router();
const request = require('request');

const signin = function({username, password}, cb) {
  request.post(
    {
      url: `${AUTH_URL}/signin`,
      form: {username,password}
    },
    (err, data, body) => {
      cb(JSON.parse(body))
    }
  )
};

const signup = function({email,password, username}, cb) {
  request.post(
    {
      url: `${AUTH_URL}/signup`,
      form: {email,password, username}
    },
    (err, data, body) => {
      cb(JSON.parse(body))
    })
};

router.post('/signin',(req, res) => {
  signin(req.body, function(data) {
    res.status(200).json(data)
  })

})

router.post('/signup',(req, res) => {
  signup(req.body, function(data) {
    res.status(200).json(data)
  })
})

module.exports = router;
