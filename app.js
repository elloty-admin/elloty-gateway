// const app = require('http').createServer()
//   , io = require('socket.io').listen(app)
//   , Auth = require('./sockets/auth')(io)

const app = require('express')();
const bodyParser = require('body-parser');
const router = require('./routes/index');
const cors = require('cors')

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/',router);


app.listen(9090, function() {
  console.log('Gateway listen on 9090')
});
